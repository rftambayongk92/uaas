import React, { Component } from 'react'
import { Text, View ,StatusBar, TextInput, FlatList} from 'react-native'

let database = [
  {indonesia: 'kamu', madura: 'kakeh'},
  {indonesia: 'ikan', madura: 'jhukok'},
  {indonesia: 'saya', madura: 'engkok'},
  {indonesia: 'kalian', madura: 'kakeh kabbhi'},
  {indonesia: 'semua', madura: 'kabbhi'},
  {indonesia: 'sendiri', madura: 'katibik'},
  {indonesia: 'makan', madura: 'ngakan'},
  {indonesia: 'minum', madura: 'ngenom'},
  {indonesia: 'duduk', madura: 'tojuk'},
  {indonesia: 'bersama', madura: 'arheng bhereng'},
  {indonesia: 'telinga', madura: 'kopeng'},
  {indonesia: 'kumpul', madura: 'akompol'},
  {indonesia: 'mau kemana', madura: 'demmak ah'},
  {indonesia: 'anjing', madura: 'patek'},
  {indonesia: 'kucing', madura: 'koceng'},
  {indonesia: 'sapi', madura: 'sapeh'},
  {indonesia: 'kambing', madura: 'embik'},
  {indonesia: 'burung', madura: 'manok'},
  {indonesia: 'tidur', madura: 'tedung'},
  {indonesia: 'bangun', madura: 'jegheh'},
  {indonesia: 'lupa', madura: 'kloppaeh'},
  {indonesia: 'ingat', madura: 'enghak'},
  {indonesia: 'cabe', madura: 'cabbih'},
  {indonesia: 'kaki', madura: 'sokoh'},
  {indonesia: 'tangan', madura: 'tanang'},
  {indonesia: 'jari', madura: 'tonjhuk'},
  {indonesia: 'mata', madura: 'matah'},
  {indonesia: 'kepala', madura: 'cetak'},
  {indonesia: 'leher', madura: 'lek er'},
  {indonesia: 'dada', madura: 'dedeh'},
  {indonesia: 'gigi', madura: 'ghighih'},
  {indonesia: 'malam', madura: 'malem'},
  {indonesia: 'timur', madura: 'temor'},
  {indonesia: 'barat', madura: 'bherek'},
  {indonesia: 'selatan', madura: 'laok'},
  {indonesia: 'utara', madura: 'dhejeh'},
  {indonesia: 'rumah', madura: 'roma'},
  {indonesia: 'halaman', madura: 'tanian'},
  {indonesia: 'kotoran sapi', madura: 'celattung'},
  {indonesia: 'kotoran ayam', madura: 'mancok'},
  {indonesia: 'krikil', madura: 'bliker'},
  {indonesia: 'batu', madura: 'betoh'},
  {indonesia: 'pasir', madura: 'bheddhih'},
  {indonesia: 'lampu', madura: 'dhemar'},
  {indonesia: 'tangga', madura: 'andhek'},
  {indonesia: 'laki laki', madura: 'lakek'},
  {indonesia: 'perempuan', madura: 'binik'},
]

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      data: database
    };
  }

  search = () => {
    let data = database;

    data = data.filter(item => item.indonesia.toLocaleLowerCase().includes(this.state.text.toLocaleLowerCase()));

    this.setState({
      data: data
    })
  }
  render() {
    return (
      <View style={{flex: 1}}>
        <StatusBar backgroundColor="#0288d1" barStyle="light-content" />

        <View style={{ padding: 20, backgroundColor: '#03a9f4' }}>
        <Text style= {{textAlign: 'center', color: '#FFFFFF', fontWeight: 'bold',}}>KAMUS MADURA</Text>
      </View>

      <TextInput
        style={{height: 40, borderColor: 'gray', borderWidth: 1, paddingLeft: 10, marginVertical: 20, marginHorizontal: 10, borderRadius: 50}}
        onChangeText={text => this.setState({text: text})}
        value={this.state.text}
        placeholder='Apa Yang Kau Tak Mengerti Tentang Bhs Madura ?'
        onKeyPress={() => this.search()}

      />

      <FlatList
        data={this.state.data}
        renderItem={({item}) =>
        <View style={{borderWidth: 1, borderRadius: 3, marginVertical: 5, marginHorizontal: 20, padding: 10}}>
          <Text style={{fontSize: 18, fontWeight: 'bold'}}>{item.indonesia}</Text>
          <Text style={{fontSize: 16, marginTop: 5}}>{item.madura}</Text>
        </View>
        }
        keyExtractor={item => item.indonesia}
      />

      </View>
    );
  }
}

export default App